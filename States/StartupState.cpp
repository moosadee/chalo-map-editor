#include "StartupState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"

StartupState::StartupState()
{
}

void StartupState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "StartupState::Init", "function-trace" );
    IState::Init( name );
}

void StartupState::Setup()
{
    chalo::Logger::Out( "", "StartupState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );

    chalo::MenuManager::LoadTextMenu( "startup.chalomenu" );

    // Add buttons for loadable maps
    std::map< std::string, std::string > savedMaps = chalo::ConfigManager::GetPartial( "saved-map" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );
    for ( auto& savedMap : savedMaps )
    {
        chalo::MenuManager::AddButton(
            "dynamic",
            "btnLoad_" + savedMap.second,
            pos,
            dimensions,
            "button-long",
            sf::Vector2f( 0, 0 ),
            "main",
            20,
            sf::Color::White,
            sf::Vector2f( 10, 0 ),
            savedMap.second
            );

            pos.y += dimensions.height + 10;
            if ( pos.y > 850 )
            {
                pos.y = 700;
                pos.x += dimensions.width + 10;
            }
    }

    chalo::InputManager::Setup();
}

void StartupState::Cleanup()
{
    chalo::Logger::Out( "", "StartupState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void StartupState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btnNewMap" )
    {
        CreateNewMap();
    }
    else if ( clickedButton == "btnRecentMap" )
    {
        LoadRecentMap();
    }
    else if ( chalo::StringUtility::Contains( clickedButton, "btnLoad_" ) )
    {
        LoadMap( clickedButton );
    }
}

void StartupState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}


void StartupState::CreateNewMap()
{
    std::string newMapName      = chalo::MenuManager::GetTextboxValue( "txtNewMapName" );
    std::string newMapWidth     = chalo::MenuManager::GetTextboxValue( "txtNewMapWidth" );
    std::string newMapHeight    = chalo::MenuManager::GetTextboxValue( "txtNewMapHeight" );
    std::string newMapTileWidth = chalo::MenuManager::GetTextboxValue( "txtNewMapTileWidth" );
    std::string mapTileset      = chalo::MenuManager::GetTextboxValue( "txtNewMapTileset" );

    if ( newMapName == "" )
    {
        // Set default name
        newMapName = "default";
    }

    if ( mapTileset == "" )
    {
        mapTileset = "tileset";
    }

    chalo::Logger::Out( "Create new map. Name: " + newMapName
        + ", map width: " + newMapWidth + ", map height: " + newMapHeight
        + ", tile width: " + newMapTileWidth
        + ", tileset: " + mapTileset
        , "StartupState::CreateNewMap" );

    chalo::ConfigManager::Set( "last-edited-map", newMapName );
    chalo::ConfigManager::Set( "last-edited-map-width", newMapWidth );
    chalo::ConfigManager::Set( "last-edited-map-height", newMapHeight );
    chalo::ConfigManager::Set( "last-edited-map-tile-width", newMapTileWidth );
    chalo::ConfigManager::Set( "last-edited-map-tileset", newMapTileWidth );

    chalo::Messager::Set( "map-name", newMapName );
    chalo::Messager::Set( "map-width", newMapWidth );
    chalo::Messager::Set( "map-height", newMapHeight );
    chalo::Messager::Set( "map-tile-width", newMapTileWidth );
    chalo::Messager::Set( "map-tileset", mapTileset );
    chalo::Messager::Set( "init-state", "new-map" );

    m_gotoState = "editorstate";
}

void StartupState::LoadRecentMap()
{
    std::string newMapName      = chalo::ConfigManager::Get( "last-edited-map" );
    std::string newMapWidth     = chalo::ConfigManager::Get( "last-edited-map-width" );
    std::string newMapHeight    = chalo::ConfigManager::Get( "last-edited-map-height" );
    std::string newMapTileWidth = chalo::ConfigManager::Get( "last-edited-map-tile-width" );
    std::string mapTileset      = chalo::ConfigManager::Get( "last-edited-map-tileset" );

    chalo::Messager::Set( "map-name", newMapName );
    chalo::Messager::Set( "map-width", newMapWidth );
    chalo::Messager::Set( "map-height", newMapHeight );
    chalo::Messager::Set( "map-tile-width", newMapTileWidth );
    chalo::Messager::Set( "map-tileset", newMapTileWidth );
    chalo::Messager::Set( "init-state", "load-recent-map" );

    m_gotoState = "editorstate";
}

void StartupState::LoadMap( const std::string& buttonName )
{
    int stringBegin = buttonName.find( "_" ) + 1;
    int length = buttonName.size() - stringBegin;

    std::string mapName = buttonName.substr( stringBegin, length );

    chalo::Logger::Out( "Load map: \"" + mapName + "\"", "StartupState::LoadMap" );

    chalo::ConfigManager::Set( "last-edited-map",   mapName );

    chalo::Messager::Set( "map-name",       mapName );
    chalo::Messager::Set( "init-state",     "load-map" );

    m_gotoState = "editorstate";
}

