#include "EditorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/UIManager.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/GameObjects/Tile.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"

EditorState::EditorState()
{
}

void EditorState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "EditorState:Init", "function-trace" );
    chalo::IState::Init( name );
    m_mapPath = "SavedMaps/";
}

void EditorState::Setup()
{
    chalo::Logger::Out( "", "EditorState::Setup", "function-trace" );
    chalo::IState::Setup();

    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );

    chalo::MenuManager::LoadTextMenu( "editor.chalomenu" );

    chalo::InputManager::Setup();

//    chalo::UIManager::Clear();

    bool debugView = false;
    chalo::DrawManager::SetDebugView( debugView );

    chalo::Logger::Out( "Debug view: " + chalo::StringUtility::BooleanToString( debugView ), "EditorState::Setup" );

    m_mapName = chalo::Messager::Get( "map-name" );

    chalo::Logger::Out( "Map name: " + m_mapName, "EditorState::Setup" );

    std::string initState = chalo::Messager::Get( "init-state" );

    chalo::Logger::Out( "Init state: " + initState, "EditorState::Setup" );

    if ( initState == "new-map" )
    {
        // Load tileset based on messager info
        m_tilesetName = chalo::Messager::Get( "map-tileset" );
        chalo::TextureManager::Add( "tileset",           "Content/Graphics/Tilesets/" + m_tilesetName + ".png" );
        chalo::TextureManager::Add( "tileset-placements","Content/Graphics/Tilesets/" + m_tilesetName + "-placements.png" );
        chalo::TextureManager::Add( "tileset-shadows",   "Content/Graphics/Tilesets/" + m_tilesetName + "-shadows.png" );
        chalo::TextureManager::Add( "tileset-collisions","Content/Graphics/Tilesets/" + m_tilesetName + "-collision.png" );

        m_tileWidthHeight = chalo::StringUtility::StringToInteger( chalo::Messager::Get( "map-tile-width" ) );
        m_mapWidth = chalo::StringUtility::StringToInteger( chalo::Messager::Get( "map-width" ) );
        m_mapHeight = chalo::StringUtility::StringToInteger( chalo::Messager::Get( "map-height" ) );
        m_map.SetMapDimensions( m_mapWidth, m_mapHeight );

        m_map.SetTilesets(
            m_tilesetName + ".png",
            m_tilesetName + "-collision.png",
            m_tilesetName + "-shadows.png",
            m_tilesetName + "-placements.png"
        );

        SaveMap( m_mapName );
    }
    else if ( initState == "load-recent-map" )
    {
        m_map.LoadTextMap( m_mapPath, m_mapName );

        chalo::ConfigManager::Set( "last-edited-map-width",         m_map.GetWidth() );
        chalo::ConfigManager::Set( "last-edited-map-height",        m_map.GetHeight() );
        chalo::ConfigManager::Set( "last-edited-map-tile-width",    m_map.GetTileWidth() );
        chalo::ConfigManager::Set( "last-edited-map-tileset",       m_map.GetTilesetName() );

        chalo::Messager::Set( "map-width",      m_map.GetWidth() );
        chalo::Messager::Set( "map-height",     m_map.GetHeight() );
        chalo::Messager::Set( "map-tile-width", m_map.GetTileWidth() );
        chalo::Messager::Set( "map-tileset",    m_map.GetTilesetName() );

        m_tileWidthHeight = m_map.GetTileWidth();
    }
    else if ( initState == "load-map" )
    {
        // Will load data from the map file
        m_map.LoadTextMap( m_mapPath, m_mapName );

        chalo::ConfigManager::Set( "last-edited-map-width",         m_map.GetWidth() );
        chalo::ConfigManager::Set( "last-edited-map-height",        m_map.GetHeight() );
        chalo::ConfigManager::Set( "last-edited-map-tile-width",    m_map.GetTileWidth() );
        chalo::ConfigManager::Set( "last-edited-map-tileset",       m_map.GetTilesetName() );

        chalo::Messager::Set( "map-width",      m_map.GetWidth() );
        chalo::Messager::Set( "map-height",     m_map.GetHeight() );
        chalo::Messager::Set( "map-tile-width", m_map.GetTileWidth() );
        chalo::Messager::Set( "map-tileset",    m_map.GetTilesetName() );

        m_tileWidthHeight = m_map.GetTileWidth();
    }

    m_brushSizesWidth.push_back( 1 );
    m_brushSizesWidth.push_back( 3 );
    m_brushSizesWidth.push_back( 5 );
    m_brushSizesWidth.push_back( 10 );
    m_brushSizesWidth.push_back( 40 );
    m_brushSizesWidth.push_back( 80 );

    m_brushSizesHeight.push_back( 1 );
    m_brushSizesHeight.push_back( 3 );
    m_brushSizesHeight.push_back( 5 );
    m_brushSizesHeight.push_back( 10 );
    m_brushSizesHeight.push_back( 20 );
    m_brushSizesHeight.push_back( 45 );

    m_gridEnabled = true;

    m_statusCounterMax = 1000;
    m_statusCounter = 0;

    m_saveCounterMax = 1000;
    m_saveCounter = m_saveCounterMax;

    SetupUI();
    GotoTilesetPanel();
    UpdateUI();
}

void EditorState::Cleanup()
{
    chalo::Logger::Out( "", "EditorState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void EditorState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    // Make a backup save
    if ( m_saveCounter > 0 )
    {
        m_saveCounter -= 1;
        if ( m_saveCounter <= 0 )
        {
            SaveMap( "backup.chalomap" );
            m_saveCounter = m_saveCounterMax;
            chalo::UIManager::GetLabel( "lbllast-save" ).SetText( "Last backup at " + chalo::StringUtility::GetTime() );
        }
    }

    // Clear status after a little while
    if ( m_statusCounter > 0 )
    {
        m_statusCounter -= 1;
        if ( m_statusCounter <= 0 )
        {
            ClearStatus();
        }
    }

    HandleKeyboardShortcuts();
    HandleMouse();
}

void EditorState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();

//    chalo::DrawManager::AddMap( m_map, m_brush.layer, m_brush.layerType );
//
//    for ( auto& label : chalo::UIManager::GetLabels() )
//    {
//        chalo::DrawManager::AddUILabel( label.second );
//    }
//
//    for ( auto& image : chalo::UIManager::GetImages() )
//    {
//        chalo::DrawManager::AddUIImage( image.second );
//    }
//
//    for ( auto& button : chalo::UIManager::GetButtons() )
//    {
//        chalo::DrawManager::AddUIButton( button.second );
//    }
//
//    if ( m_gridEnabled )
//    {
//      for ( auto& shape : chalo::UIManager::GetRectangles() )
//      {
//          chalo::DrawManager::AddDebugRectangle( shape.second.GetShape() );
//      }
//    }
//
//    chalo::DrawManager::AddDebugRectangle( m_selectedBrush );
}

void EditorState::SaveMap( const std::string& mapName )
{
    chalo::Logger::Out( "Parameters - mapName: " + mapName, "EditorState:SaveMap", "function-trace" );

    m_map.SaveTextMap( m_mapPath, mapName + ".chalomap" );
    SetStatus( "Saved map to \"" + m_mapPath + mapName + "\"." );
}

void EditorState::SetKeyboardCooldown()
{
    m_keyboardCooldown = 10;
}

void EditorState::SetButtonCooldown()
{
    m_buttonCooldown = 10;
}

void EditorState::PlaceTile( sf::Vector2f position, bool snapToGrid /* = true */ )
{
    chalo::Logger::Out( "Parameters - position: " + chalo::StringUtility::CoordinateToString( position ) + ", snapToGrid: " + chalo::StringUtility::BooleanToString( snapToGrid ), "EditorState:PlaceTile", "function-trace" );
    // Figure out closest grid area
    // m_tileWidthHeight
    sf::Vector2f tilePosition;

    if ( snapToGrid )
    {
        tilePosition.x = int( position.x / m_tileWidthHeight ) * m_tileWidthHeight;
        tilePosition.y = int( position.y / m_tileWidthHeight ) * m_tileWidthHeight;
    }
    else
    {
        tilePosition = position;
    }

    for ( int brushWidth = 0; brushWidth < m_brush.dimensions.x; brushWidth++ )
    {
        for ( int brushHeight = 0; brushHeight < m_brush.dimensions.y; brushHeight++ )
        {
            int x = tilePosition.x + ( brushWidth * m_tileWidthHeight );
            int y = tilePosition.y + ( brushHeight * m_tileWidthHeight );

            if ( m_brush.layerType == chalo::BELOWTILELAYER )
            {
                chalo::Map::Tile tile;
                tile.Setup( x, y, chalo::StringUtility::GetTime() );
                tile.SetTexture( chalo::TextureManager::Get( "tileset" ), m_brush.imageClip );
                tile.SetLayerIndex( m_brush.layer );
                m_map.AddTile( m_brush.layer, tile, chalo::BELOWTILELAYER );
            }

            else if ( m_brush.layerType == chalo::ABOVETILELAYER )
            {
                chalo::Map::Tile tile;
                tile.Setup( x, y, chalo::StringUtility::GetTime() );
                tile.SetTexture( chalo::TextureManager::Get( "tileset" ), m_brush.imageClip );
                tile.SetLayerIndex( m_brush.layer );
                m_map.AddTile( m_brush.layer, tile, chalo::ABOVETILELAYER );
            }

            else if ( m_brush.layerType == chalo::SHADOWLAYER )
            {
                chalo::Map::Shadow shadow;
                shadow.Setup( x, y, chalo::StringUtility::GetTime() );
                shadow.SetTexture( chalo::TextureManager::Get( "tileset-shadows" ), m_brush.imageClip );
                shadow.SetLayerIndex( m_brush.layer );
                m_map.AddShadow( m_brush.layer, shadow );
            }

            else if ( m_brush.layerType == chalo::COLLISIONLAYER )
            {
                sf::IntRect rect = sf::IntRect( 0, 0, m_tileWidthHeight, m_tileWidthHeight );
                chalo::Map::Collision collision;
                collision.Setup( x, y, chalo::StringUtility::GetTime() );
                collision.SetObjectCollisionRectangle( rect );
                collision.SetTexture( chalo::TextureManager::Get( "tileset-collision" ), m_brush.imageClip );
                chalo::Logger::Out( "Collision rectangle: " + chalo::StringUtility::RectangleToString( m_brush.imageClip ), "EditorState::PlaceTile" );
                m_map.AddCollision( collision );
            }

            else if ( m_brush.layerType == chalo::PLACEMENTLAYER )
            {
                chalo::Map::Placement placement;
                placement.Setup( x, y, chalo::StringUtility::GetTime() );
                placement.SetTexture( chalo::TextureManager::Get( "tileset-placements" ), m_brush.imageClip );

                // TODO: Bleh!
                int w = m_tileWidthHeight;
                if      ( m_brush.imageClip.left == 0 && m_brush.imageClip.top == 0 ) { placement.SetCharacterType( "ayda" ); }
                else if ( m_brush.imageClip.left == w*1 && m_brush.imageClip.top == 0 ) { placement.SetCharacterType( "goblin" ); }
                else if ( m_brush.imageClip.left == w*2 && m_brush.imageClip.top == 0 ) { placement.SetCharacterType( "skeleton" ); }
                else if ( m_brush.imageClip.left == w*3 && m_brush.imageClip.top == 0 ) { placement.SetCharacterType( "snake" ); }

                else if ( m_brush.imageClip.left == w*1 && m_brush.imageClip.top == w*1 ) { placement.SetCharacterType( "spider" ); }
                else if ( m_brush.imageClip.left == w*2 && m_brush.imageClip.top == w*1 ) { placement.SetCharacterType( "twohead" ); }
                else if ( m_brush.imageClip.left == w*3 && m_brush.imageClip.top == w*1 ) { placement.SetCharacterType( "troll" ); }
                else if ( m_brush.imageClip.left == w*4 && m_brush.imageClip.top == w*1 ) { placement.SetCharacterType( "dragon" ); }

                else if ( m_brush.imageClip.left == w*1 && m_brush.imageClip.top == w*2 ) { placement.SetCharacterType( "cyclops" ); }
                else if ( m_brush.imageClip.left == w*2 && m_brush.imageClip.top == w*2 ) { placement.SetCharacterType( "ghost" ); }
                else if ( m_brush.imageClip.left == w*3 && m_brush.imageClip.top == w*2 ) { placement.SetCharacterType( "demon" ); }
                else if ( m_brush.imageClip.left == w*4 && m_brush.imageClip.top == w*2 ) { placement.SetCharacterType( "bat" ); }


                m_map.AddPlacement( placement );
            }

            else if ( m_brush.layerType == chalo::WARPLAYER )
            {
                chalo::Map::Warp warp;
                m_map.AddWarp( warp );
            }
        }
    }

}

void EditorState::RemoveTile( sf::Vector2f position, bool snapToGrid /* = true */ )
{
    chalo::Logger::Out( "Parameters - position: " + chalo::StringUtility::CoordinateToString( position ) + ", snapToGrid: " + chalo::StringUtility::BooleanToString( snapToGrid ), "EditorState:RemoveTile", "function-trace" );
    sf::IntRect removeRegion;

    if ( snapToGrid )
    {
        removeRegion.left = int( position.x / m_tileWidthHeight ) * m_tileWidthHeight;
        removeRegion.top = int( position.y / m_tileWidthHeight ) * m_tileWidthHeight;
    }
    else
    {
        removeRegion.left = position.x;
        removeRegion.top = position.y;
    }


    removeRegion.width = m_brush.dimensions.x * m_tileWidthHeight;
    removeRegion.height = m_brush.dimensions.y * m_tileWidthHeight;

    m_map.RemoveTiles( m_brush.layer, removeRegion, m_brush.layerType );
}

sf::Vector2f EditorState::GetMousePosOnMap()
{
    sf::Vector2i mousePos = sf::Mouse::getPosition( chalo::Application::GetWindow() );
    sf::Vector2f pos;
    pos.x = mousePos.x - m_posMapEditor.left;
    pos.y = mousePos.y - m_posMapEditor.top;
    return pos;
}

void EditorState::UpdateUI()
{
    // TODO: This should only happen when input occurs.

    chalo::MenuManager::GetLabel( "lbltotal-tiles" ).SetText( "Total tiles: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount() ) );

    chalo::MenuManager::GetLabel( "lblundo-stack" ).SetText( "Undo stack size: " + chalo::StringUtility::IntegerToString( m_map.GetUndoSize() ) );

    chalo::MenuManager::GetLabel( "lbltotalbelow" ).SetText( "Total below: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::BELOWTILELAYER ) ) );
    chalo::MenuManager::GetLabel( "lbltotalabove" ).SetText( "Total above: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::ABOVETILELAYER ) ) );
    chalo::MenuManager::GetLabel( "lbltotalshadows" ).SetText( "Total shadows: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::SHADOWLAYER ) ) );
    chalo::MenuManager::GetLabel( "lbltotalplacements" ).SetText( "Total placements: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::PLACEMENTLAYER ) ) );
    chalo::MenuManager::GetLabel( "lbltotalcollisions" ).SetText( "Total collisions: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::COLLISIONLAYER ) ) );
    chalo::MenuManager::GetLabel( "lbltotalwarps" ).SetText( "Total warps: " + chalo::StringUtility::IntegerToString( m_map.GetTileCount( chalo::WARPLAYER ) ) );

    chalo::MenuManager::GetLabel( "lblMapDimensions" ).SetText( "Map dimensions: " + chalo::StringUtility::DimensionsToString( m_map.GetWidth(), m_map.GetHeight() ) );
    chalo::MenuManager::GetLabel( "lblTileDimensinos" ).SetText( "tile dimensions: " + chalo::StringUtility::DimensionsToString( m_tileWidthHeight, m_tileWidthHeight ) );


    // Tile type
    // Reset...
    chalo::MenuManager::GetButton( "below-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );
    chalo::MenuManager::GetButton( "above-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );
    chalo::MenuManager::GetButton( "warp-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );
    chalo::MenuManager::GetButton( "shadow-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );
    chalo::MenuManager::GetButton( "placement-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );
    chalo::MenuManager::GetButton( "collision-type" ).SetupBackground( chalo::TextureManager::Get( "button-long" ), sf::Vector2f( 0, 0 ) );

    // Set selected...
    if ( m_brush.layerType == chalo::BELOWTILELAYER )
    {
        chalo::MenuManager::GetButton( "below-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoTilesetPanel();
    }
    else if ( m_brush.layerType == chalo::ABOVETILELAYER )
    {
        chalo::MenuManager::GetButton( "above-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoTilesetPanel();
    }
    else if ( m_brush.layerType == chalo::SHADOWLAYER )
    {
        chalo::MenuManager::GetButton( "shadow-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoShadowPanel();
    }
    else if ( m_brush.layerType == chalo::WARPLAYER )
    {
        chalo::MenuManager::GetButton( "warp-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoWarpPanel();
    }
    else if ( m_brush.layerType == chalo::PLACEMENTLAYER )
    {
        chalo::MenuManager::GetButton( "placement-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoPlacementPanel();
    }
    else if ( m_brush.layerType == chalo::COLLISIONLAYER )
    {
        chalo::MenuManager::GetButton( "collision-type" ).SetupBackground( chalo::TextureManager::Get( "button-long-selected" ), sf::Vector2f( 0, 0 ) );
        GotoCollisionPanel();
    }

    // Layer info
    chalo::MenuManager::GetLabel( "current-layer" ).SetText( chalo::StringUtility::IntegerToString( m_brush.layer ) );

    // Brush size
    // Reset...
    std::string buttonName;

    for ( int i = 0; i < m_brushSizesWidth.size(); i++ )
    {
        buttonName = "brushwidth-" + chalo::StringUtility::IntegerToString( m_brushSizesWidth[i] );
        chalo::MenuManager::GetButton( buttonName ).SetupBackground( chalo::TextureManager::Get( "button-square" ), sf::Vector2f( 0, 0 ) );
    }

    for ( int i = 0; i < m_brushSizesHeight.size(); i++ )
    {
        buttonName = "brushheight-" + chalo::StringUtility::IntegerToString( m_brushSizesHeight[i] );
        chalo::MenuManager::GetButton( buttonName ).SetupBackground( chalo::TextureManager::Get( "button-square" ), sf::Vector2f( 0, 0 ) );
    }

    buttonName = "brushwidth-" + chalo::StringUtility::IntegerToString( m_brush.dimensions.x );
    chalo::MenuManager::GetButton( buttonName ).SetupBackground( chalo::TextureManager::Get( "button-square-selected" ), sf::Vector2f( 0, 0 ) );

    buttonName = "brushheight-" + chalo::StringUtility::IntegerToString( m_brush.dimensions.y );
    chalo::MenuManager::GetButton( buttonName ).SetupBackground( chalo::TextureManager::Get( "button-square-selected" ), sf::Vector2f( 0, 0 ) );

    if ( m_currentPanel == TILESETPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset" ) );
    }
    else if ( m_currentPanel == SHADOWSPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-shadows" ) );
    }
    else if ( m_currentPanel == COLLISIONPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-collisions" ) );
    }
    else if ( m_currentPanel == PLACEMENTPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-placements" ) );
    }
    else if ( m_currentPanel == WARPPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset" ) );
    }
    m_brush.sprite.setTextureRect( m_brush.imageClip );
}

void EditorState::UpdateGrid()
{
    // Highlight which grid areas the brush is over
    sf::Vector2i localPosition = sf::Mouse::getPosition( chalo::Application::GetWindow() );

    if ( chalo::PointInBoxCollision( localPosition, m_posMapEditor ) )
    {
        sf::Vector2i gridPos;
        gridPos.x = ( localPosition.x / m_tileWidthHeight ) * m_tileWidthHeight;
        gridPos.y = ( localPosition.y / m_tileWidthHeight ) * m_tileWidthHeight;

        sf::IntRect rect = sf::IntRect(
                               gridPos.x,
                               gridPos.y,
                               m_tileWidthHeight * m_brush.dimensions.x,
                               m_tileWidthHeight * m_brush.dimensions.y
                           );

        sf::RectangleShape rectangle;
        rectangle.setPosition( sf::Vector2f( rect.left, rect.top ) );
        rectangle.setSize( sf::Vector2f( rect.width, rect.height ) );
        rectangle.setFillColor( sf::Color::Transparent );
        rectangle.setOutlineColor( sf::Color( 255, 255, 0, 100 ) );
        rectangle.setOutlineThickness( 1 );
        chalo::UIManager::AddRectangle( "highlighter", rectangle );

        for ( int y = 0; y < m_brush.dimensions.y; y++ )
        {
            for ( int x = 0; x < m_brush.dimensions.x; x++ )
            {
                m_brush.sprite.setPosition( sf::Vector2f( rect.left + (x * m_tileWidthHeight), rect.top + (y * m_tileWidthHeight)) );
                chalo::DrawManager::AddSprite( m_brush.sprite );
            }
        }
    }
}

void EditorState::SetupUI()
{
    chalo::Logger::Out( "", "EditorState::SetupUI", "function-trace" );

    // Tileset buttons
    sf::Vector2u tilesetDimensions = chalo::TextureManager::Get( "tileset" ).getSize();

    sf::Vector2u dimensions = chalo::Application::GetWindow().getSize();

    chalo::Logger::Out( "Size of tileset: " + chalo::StringUtility::CoordinateToString( tilesetDimensions ), "EditorState::Setup" );
    chalo::Logger::Out( "Size of window: " + chalo::StringUtility::CoordinateToString( dimensions ), "EditorState::Setup" );
    chalo::Logger::Out( "Tile width: " + chalo::StringUtility::IntegerToString( m_tileWidthHeight ), "EditorState::Setup" );

    sf::RectangleShape rs;

    // TODO: Replace with MenuManager

    chalo::Logger::Out( "Add frames", "EditorState::Setup" );

    // FRAME - MAP AREA
    m_posMapEditor.left = 0;
    m_posMapEditor.top = 0;
    m_posMapEditor.width = 1280;
    m_posMapEditor.height = 720;

    chalo::UIManager::AddRectangle( "bg2", sf::Color::White, 1, sf::Color( 255, 255, 255, 50 ),
                                    sf::Vector2f( m_posMapEditor.left, m_posMapEditor.top ),
                                    sf::Vector2f( m_posMapEditor.width, m_posMapEditor.height )
                                  );

    // FRAME - TILESET SELECTOR
    m_posTilesetSelector.left = 1300;
    m_posTilesetSelector.top = 10;
    m_posTilesetSelector.width = dimensions.x - m_posTilesetSelector.left - 10;
    m_posTilesetSelector.height = dimensions.y - 20;

    chalo::UIManager::AddRectangle( "bg3", sf::Color::White, 1, sf::Color( 255, 255, 255, 50 ),
                                    sf::Vector2f( m_posTilesetSelector.left, m_posTilesetSelector.top ),
                                    sf::Vector2f( m_posTilesetSelector.width, m_posTilesetSelector.height )
                                  );

    // FRAME - CONTROL PANEL
    m_posControlPanel.width = m_posMapEditor.width;
    m_posControlPanel.height = dimensions.y - m_posMapEditor.height - 30;
    m_posControlPanel.left = 10;
    m_posControlPanel.top = dimensions.y - m_posControlPanel.height - 10;

    chalo::UIManager::AddRectangle( "bg1", sf::Color::White, 1, sf::Color( 255, 255, 255, 50 ),
                                    sf::Vector2f( m_posControlPanel.left, m_posControlPanel.top ),
                                    sf::Vector2f( m_posControlPanel.width, m_posControlPanel.height )
                                  );

    // Selected tileset frame
    m_selectedBrush.setPosition( sf::Vector2f( m_posTilesetSelector.left, m_posTilesetSelector.top ) );
    m_selectedBrush.setSize( sf::Vector2f( m_tileWidthHeight, m_tileWidthHeight ) );
    m_selectedBrush.setOutlineColor( sf::Color::Yellow );
    m_selectedBrush.setOutlineThickness( 2 );
    m_selectedBrush.setFillColor( sf::Color::Transparent );

    chalo::Logger::Out( "Add elements", "EditorState::Setup" );

    // Instructions
    sf::Vector2f pos;
    pos.x = 10;
    pos.y = 920 - 200;
    chalo::UIManager::AddLabel( "instruction1", chalo::FontManager::Get( "main" ), 20, sf::Color::White, sf::Vector2f( dimensions.x - 250, dimensions.y - 30 ), std::string( "Chalo Map Editor v0.1" ) );

    chalo::Logger::Out( "Add tile grid", "EditorState::Setup" );

    // Tile grid
    int counter = 0;
    for ( int y = 0; y < 720/m_tileWidthHeight; y++ )
    {
        for ( int x = 0; x < 1280/m_tileWidthHeight; x++ )
        {
            chalo::Logger::Out( "(x,y): " + chalo::StringUtility::CoordinateToString( x, y ), "EditorState::Setup" );

            int xpos = ( x * m_tileWidthHeight ) + m_posMapEditor.left;
            int ypos = ( y * m_tileWidthHeight ) + m_posMapEditor.top;

            // AddRectangleShape( const std::string& layerName, const std::string& elementName, sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f dimensions )
            chalo::MenuManager::AddRectangleShape( "mapLayer", "grid-" + chalo::StringUtility::IntegerToString( counter ),
                sf::Color( 255, 255, 255, 25 ), 1, sf::Color::Transparent, sf::Vector2f( xpos, ypos ), sf::Vector2f( m_tileWidthHeight, m_tileWidthHeight )
                );
            counter++;
        }
    }
}

void EditorState::SetStatus( const std::string& status )
{
    chalo::Logger::Out( "Parameters - status: " + status, "EditorState:SetStatus", "function-trace" );
    chalo::UIManager::GetLabel( "lblstatus" ).SetText( status );
    m_statusCounter = m_statusCounterMax;
}

void EditorState::ClearStatus()
{
    chalo::Logger::Out( "", "EditorState::ClearStatus", "function-trace" );
    chalo::UIManager::GetLabel( "lblstatus" ).SetText( "..." );
    m_statusCounter = 0;
}

void EditorState::SetBrush( sf::IntRect imageClip )
{
    chalo::Logger::Out( "Parameters - imageClip: " + chalo::StringUtility::RectangleToString( imageClip ), "EditorState:SetBrush", "function-trace" );

    if ( m_currentPanel == TILESETPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset" ) );
    }
    else if ( m_currentPanel == SHADOWSPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-shadows" ) );
    }
    else if ( m_currentPanel == COLLISIONPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-collisions" ) );
    }
    else if ( m_currentPanel == PLACEMENTPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset-placements" ) );
    }
    else if ( m_currentPanel == WARPPANEL )
    {
        m_brush.SetTexture( chalo::TextureManager::Get( "tileset" ) );
    }

    m_brush.imageClip = imageClip;
    m_brush.sprite.setTextureRect( m_brush.imageClip );
}

void EditorState::ShiftBrush( chalo::Direction direction )
{
    chalo::Logger::Out( "Parameters - direction: " + chalo::StringUtility::IntegerToString( direction ), "EditorState::ShiftBrush", "function-trace" );
    if ( direction == chalo::NORTH )
    {
        m_brush.imageClip.top -= m_brush.imageClip.height;
    }
    else if ( direction == chalo::SOUTH )
    {
        m_brush.imageClip.top += m_brush.imageClip.height;
    }
    else if ( direction == chalo::WEST )
    {
        m_brush.imageClip.left -= m_brush.imageClip.width;
    }
    else if ( direction == chalo::EAST )
    {
        m_brush.imageClip.left += m_brush.imageClip.width;
    }

    SetBrush( m_brush.imageClip );

    m_selectedBrush.setPosition( sf::Vector2f(
                                     m_brush.imageClip.left + m_posTilesetSelector.left,
                                     m_brush.imageClip.top + m_posTilesetSelector.top
                                 ) );
}

void EditorState::ClearPanels()
{
    chalo::Logger::Out( "", "EditorState:ClearPanels", "function-trace" );
    chalo::UIManager::RemoveElementsWith( "PANEL" );
}

void EditorState::GotoTilesetPanel()
{
    chalo::Logger::Out( "", "EditorState::GotoTilesetPanel", "function-trace" );
    ClearPanels();
    m_currentPanel = TILESETPANEL;

    std::string tileset = "tileset";
    sf::Vector2u tilesetDimensions = chalo::TextureManager::Get( tileset ).getSize();

    chalo::Logger::Out( "Tileset width: " + chalo::StringUtility::IntegerToString( tilesetDimensions.x ) );

    for ( int y = 0; y < tilesetDimensions.y / m_tileWidthHeight; y++ )
    {
        for ( int x = 0; x < tilesetDimensions.x / m_tileWidthHeight; x++ )
        {
            std::string buttonName = "TILESETPANEL-tileset-"
                                     + chalo::StringUtility::IntegerToString( x ) + "-"
                                     + chalo::StringUtility::IntegerToString( y );
            chalo::UIButton tileButton;

            int xpos = ( x * m_tileWidthHeight ) + m_posTilesetSelector.left;
            int ypos = ( y * m_tileWidthHeight ) + m_posTilesetSelector.top;

            tileButton.Setup(
                buttonName,
                sf::Vector2f( xpos, ypos ),
                sf::IntRect( 0, 0, m_tileWidthHeight, m_tileWidthHeight )
            );

            tileButton.SetupIcon(
                chalo::TextureManager::Get( tileset ),
                sf::Vector2f( 0, 0 ),
                sf::IntRect( x * m_tileWidthHeight, y * m_tileWidthHeight, m_tileWidthHeight, m_tileWidthHeight )
            );

            chalo::UIManager::AddButton( buttonName, tileButton );
        }
    }
}

void EditorState::GotoPlacementPanel()
{
    chalo::Logger::Out( "", "EditorState:GotoPlacementPanel", "function-trace" );
    ClearPanels();
    m_currentPanel = PLACEMENTPANEL;

    std::string tileset = "tileset-placements";
    sf::Vector2u tilesetDimensions = chalo::TextureManager::Get( tileset ).getSize();

    for ( int y = 0; y < tilesetDimensions.y / m_tileWidthHeight; y++ )
    {
        for ( int x = 0; x < tilesetDimensions.x / m_tileWidthHeight; x++ )
        {
            std::string buttonName = "PLACEMENTPANEL-tileset-"
                                     + chalo::StringUtility::IntegerToString( x ) + "-"
                                     + chalo::StringUtility::IntegerToString( y );
            chalo::UIButton tileButton;

            int xpos = ( x * m_tileWidthHeight ) + m_posTilesetSelector.left;
            int ypos = ( y * m_tileWidthHeight ) + m_posTilesetSelector.top;

            tileButton.Setup(
                buttonName,
                sf::Vector2f( xpos, ypos ),
                sf::IntRect( 0, 0, m_tileWidthHeight, m_tileWidthHeight )
            );

            tileButton.SetupIcon(
                chalo::TextureManager::Get( tileset ),
                sf::Vector2f( 0, 0 ),
                sf::IntRect( x * m_tileWidthHeight, y * m_tileWidthHeight, m_tileWidthHeight, m_tileWidthHeight )
            );

            chalo::UIManager::AddButton( buttonName, tileButton );
        }
    }
}

void EditorState::GotoCollisionPanel()
{
    chalo::Logger::Out( "", "EditorState:GotoCollisionPanel", "function-trace" );
    ClearPanels();
    m_currentPanel = COLLISIONPANEL;

    std::string tileset = "tileset-collisions";
    sf::Vector2u tilesetDimensions = chalo::TextureManager::Get( tileset ).getSize();

    for ( int y = 0; y < tilesetDimensions.y / m_tileWidthHeight; y++ )
    {
        for ( int x = 0; x < tilesetDimensions.x / m_tileWidthHeight; x++ )
        {
            std::string buttonName = "COLLISIONPANEL-tileset-"
                                     + chalo::StringUtility::IntegerToString( x ) + "-"
                                     + chalo::StringUtility::IntegerToString( y );
            chalo::UIButton tileButton;

            int xpos = ( x * m_tileWidthHeight ) + m_posTilesetSelector.left;
            int ypos = ( y * m_tileWidthHeight ) + m_posTilesetSelector.top;

            tileButton.Setup(
                buttonName,
                sf::Vector2f( xpos, ypos ),
                sf::IntRect( 0, 0, m_tileWidthHeight, m_tileWidthHeight )
            );

            tileButton.SetupIcon(
                chalo::TextureManager::Get( tileset ),
                sf::Vector2f( 0, 0 ),
                sf::IntRect( x * m_tileWidthHeight, y * m_tileWidthHeight, m_tileWidthHeight, m_tileWidthHeight )
            );

            chalo::UIManager::AddButton( buttonName, tileButton );
        }
    }
}

void EditorState::GotoShadowPanel()
{
    chalo::Logger::Out( "", "EditorState:GotoShadowPanel", "function-trace" );
    ClearPanels();
    m_currentPanel = SHADOWSPANEL;

    std::string tileset = "tileset-shadows";
    sf::Vector2u tilesetDimensions = chalo::TextureManager::Get( tileset ).getSize();

    for ( int y = 0; y < tilesetDimensions.y / m_tileWidthHeight; y++ )
    {
        for ( int x = 0; x < tilesetDimensions.x / m_tileWidthHeight; x++ )
        {
            std::string buttonName = "SHADOWPANEL-tileset-"
                                     + chalo::StringUtility::IntegerToString( x ) + "-"
                                     + chalo::StringUtility::IntegerToString( y );
            chalo::UIButton tileButton;

            int xpos = ( x * m_tileWidthHeight ) + m_posTilesetSelector.left;
            int ypos = ( y * m_tileWidthHeight ) + m_posTilesetSelector.top;

            tileButton.Setup(
                buttonName,
                sf::Vector2f( xpos, ypos ),
                sf::IntRect( 0, 0, m_tileWidthHeight, m_tileWidthHeight )
            );

            tileButton.SetupIcon(
                chalo::TextureManager::Get( tileset ),
                sf::Vector2f( 0, 0 ),
                sf::IntRect( x * m_tileWidthHeight, y * m_tileWidthHeight, m_tileWidthHeight, m_tileWidthHeight )
            );

            chalo::UIManager::AddButton( buttonName, tileButton );
        }
    }
}

void EditorState::GotoWarpPanel()
{
    chalo::Logger::Out( "", "EditorState:GotoWarpPanel", "function-trace" );
    ClearPanels();
    m_currentPanel = WARPPANEL;
}

void EditorState::HandleMouse()
{
    bool updateAction = false;

    if ( updateAction == true )
    {
        UpdateUI();
    }
}

void EditorState::HandleKeyboardShortcuts()
{
    bool updateAction = false;

    if ( updateAction == true )
    {
        UpdateUI();
    }
}
