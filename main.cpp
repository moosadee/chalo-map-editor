#include <SFML/Graphics.hpp>

#include "chalo-engine/Application/Application.hpp"
#include "chalo-engine/Managers/StateManager.hpp"
#include "chalo-engine/Managers/MenuManager.hpp"
#include "chalo-engine/Managers/ConfigManager.hpp"
#include "chalo-engine/Utilities/Logger.hpp"
#include "chalo-engine/Utilities/Messager.hpp"

#include "States/EditorState.hpp"
#include "States/StartupState.hpp"

int main( int argumentCount, char* arguments[] )
{
    chalo::Logger::Setup();

    chalo::ConfigManager::Setup( "config.chaloconfig" );

    std::string tileSize = "16";
    std::string mapFileName = "map.moo";
    std::string tileset = "tileset-small";

    for ( int i = 0; i < argumentCount; i++ )
    {
        std::string arg = std::string( arguments[i] );
        chalo::Logger::Out( "- " + chalo::StringUtility::IntegerToString( i ) + ": " + arg
        , "main"
            );
    }

    chalo::Application::Setup( "Chalo Map Editor (Moosader)", 1920, 1080 );
    chalo::StateManager stateManager;

    chalo::MenuManager::Setup( "Content/Menus/" );

    chalo::IState* editorState = new EditorState;
    chalo::IState* startupState = new StartupState;

    chalo::FontManager::Add( "main", "Content/Fonts/mononoki-Bold.ttf" );

    stateManager.InitManager();
    stateManager.AddState( "editorstate", editorState );
    stateManager.AddState( "startupstate", startupState );
    stateManager.ChangeState( "startupstate" );

    // Debugging
//
//    chalo::Messager::Set( "map-name",       "Area1Garden.chalomap" );
//    chalo::Messager::Set( "init-state",     "load-map" );
//    stateManager.ChangeState( "editorstate" );

    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        // Updates
        chalo::Application::Update();
        stateManager.UpdateState();

        if ( stateManager.GetGotoState() != "" )
        {
            stateManager.ChangeState( stateManager.GetGotoState() );
        }

        // Drawing
        stateManager.DrawState( chalo::Application::GetWindow() );
        chalo::Application::EndDrawing();
    }

    chalo::Application::Teardown();
    chalo::Logger::Cleanup();

    return 0;
}
