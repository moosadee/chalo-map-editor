#ifndef _EDITORSTATE
#define _EDITORSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include "../Editor/Brush.hpp"

#include <vector>

enum Panel { TILESETPANEL, SHADOWSPANEL, COLLISIONPANEL, PLACEMENTPANEL, WARPPANEL };

class EditorState : public chalo::IState
{
public:
    EditorState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string m_tilesetName;
    std::string m_mapName;
    int m_tileWidthHeight;
    int m_mapWidth, m_mapHeight;

    sf::RectangleShape m_selectedBrush;
    Brush m_brush;
    chalo::Map::WritableMap m_map;

    sf::IntRect m_posMapEditor;
    sf::IntRect m_posTilesetSelector;
    sf::IntRect m_posControlPanel;

    int m_keyboardCooldown; // TODO: This should go in a keyboard manager
    int m_buttonCooldown;
    int m_saveCounter;
    int m_saveCounterMax;
    int m_statusCounter;    // TODO: These counters should be a struct or something.
    int m_statusCounterMax;

    std::string m_mapPath;
    bool m_gridEnabled;

    std::vector<int> m_brushSizesWidth;
    std::vector<int> m_brushSizesHeight;

    void SaveMap( const std::string& mapName );
    void ShiftBrush( chalo::Direction direction );

    void SetupUI();
    void UpdateUI();
    void UpdateGrid();
    void HandleMouse();
    void HandleKeyboardShortcuts();
    void SetKeyboardCooldown();
    void SetButtonCooldown();

    sf::Vector2f GetMousePosOnMap();

    void SetBrush( sf::IntRect imageClip );
    void SetLayer( int layer );
    void SetLayerType( chalo::LayerType type );
    void PlaceTile( sf::Vector2f position, bool snapToGrid = true );
    void RemoveTile( sf::Vector2f position, bool snapToGrid = true );

    void SetStatus( const std::string& status );
    void ClearStatus();

    void ClearPanels();
    void GotoTilesetPanel();
    void GotoPlacementPanel();
    void GotoWarpPanel();
    void GotoCollisionPanel();
    void GotoShadowPanel();
    Panel m_currentPanel;
};

#endif
